<?php
header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE, OPTIONS');
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token, Authorization, Accept,charset,boundary,Content-Length');

use App\Http\Controllers\Api\AuthenticateController;
use App\Http\Controllers\Api\DepartmentController;
use App\Http\Controllers\Api\ModuleController;
use App\Http\Controllers\Api\OccupationController;
use App\Http\Controllers\Api\PermissionController;
use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\UnitController;
use App\Http\Controllers\Api\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/***** AUTHENTICATION ******/
Route::post('/authenticate', [AuthenticateController::class, 'authenticate']);

//////////*********** JWT.AUTH ***********///////////
Route::group(['middleware' => ['jwt.auth']], function () {
    /***** AUTHENTICATION ******/
    Route::get('/auth-user', [AuthenticateController::class, 'authUser']);

    /***** USER ******/
    Route::middleware('permission:show-user')->get('/user', [UserController::class, 'index']);
    Route::middleware('permission:show-user')->get('/user/gettabledata/{limit}', [UserController::class, 'getTableData']);
    Route::middleware('permission:show-user')->get('/user/{id}', [UserController::class, 'show']);
    Route::middleware('permission:create-user')->post('/user', [UserController::class, 'store']);
    Route::middleware('permission:update-user')->put('/user/{id}', [UserController::class, 'update']);
    Route::middleware('permission:delete-user')->delete('/user/{id}', [UserController::class, 'destroy']);

    /***** ROLE ******/
    Route::middleware('permission:show-role')->get('/role', [RoleController::class, 'index']);
    Route::middleware('permission:show-role')->get('/role/{id}', [RoleController::class, 'show']);
    Route::middleware('permission:create-role')->post('/role', [RoleController::class, 'store']);
    Route::middleware('permission:update-role')->put('/role/{id}', [RoleController::class, 'update']);
    Route::middleware('permission:delete-role')->delete('/role/{id}', [RoleController::class, 'destroy']);

    /***** UNIT ******/
    Route::middleware('permission:show-data-master')->get('/unit', [UnitController::class, 'index']);
    Route::middleware('permission:show-data-master')->get('/unit/{id}', [UnitController::class, 'show']);
    Route::middleware('permission:create-data-master')->post('/unit', [UnitController::class, 'store']);
    Route::middleware('permission:update-data-master')->put('/unit/{id}', [UnitController::class, 'update']);
    Route::middleware('permission:delete-data-master')->delete('/unit/{id}', [UnitController::class, 'destroy']);

    /***** OCCUPATION ******/
    Route::middleware('permission:show-data-master')->get('/occupation', [OccupationController::class, 'index']);
    Route::middleware('permission:show-data-master')->get('/occupation/{id}', [OccupationController::class, 'show']);
    Route::middleware('permission:create-data-master')->post('/occupation', [OccupationController::class, 'store']);
    Route::middleware('permission:update-data-master')->put('/occupation/{id}', [OccupationController::class, 'update']);
    Route::middleware('permission:delete-data-master')->delete('/occupation/{id}', [OccupationController::class, 'destroy']);

    /***** DEPARTMENT ******/
    Route::middleware('permission:show-data-master')->get('/department', [DepartmentController::class, 'index']);
    Route::middleware('permission:show-data-master')->get('/department/{id}', [DepartmentController::class, 'show']);
    Route::middleware('permission:create-data-master')->post('/department', [DepartmentController::class, 'store']);
    Route::middleware('permission:update-data-master')->put('/department/{id}', [DepartmentController::class, 'update']);
    Route::middleware('permission:delete-data-master')->delete('/department/{id}', [DepartmentController::class, 'destroy']);

    /***** MODULE ******/
    Route::middleware('permission:show-data-master')->get('/module', [ModuleController::class, 'index']);
    Route::middleware('permission:show-data-master')->get('/module/{id}', [ModuleController::class, 'show']);
    Route::middleware('permission:create-data-master')->post('/module', [ModuleController::class, 'store']);
    Route::middleware('permission:update-data-master')->put('/module/{id}', [ModuleController::class, 'update']);
    Route::middleware('permission:delete-data-master')->delete('/module/{id}', [ModuleController::class, 'destroy']);

    /***** PERMISSION ******/
    Route::middleware('permission:show-data-master')->get('/permission/authorization-by-role/{role_id}', [PermissionController::class, 'authorizationList']);
    Route::middleware('permission:show-data-master')->get('/permission', [PermissionController::class, 'index'])->name('permission');
    Route::middleware('permission:show-data-master')->get('/permission/{id}', [PermissionController::class, 'show'])->name('permission.show');
    Route::middleware('permission:update-data-master')->post('/permission/attachpermission', [PermissionController::class, 'attachPermission'])->name('permission.attachperm');
    Route::middleware('permission:update-data-master')->post('/permission/detachpermission', [PermissionController::class, 'detachPermission'])->name('permission.detachperm');
});
