/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50711
 Source Host           : localhost:3306
 Source Schema         : covid_mnc

 Target Server Type    : MySQL
 Target Server Version : 50711
 File Encoding         : 65001

 Date: 03/12/2020 19:07:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for menus
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `description` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `icon` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `icon_child` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `seq` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `menus_name_unique`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES (1, 0, 1, 'dashboard', 'Dashboard', 'home', NULL, 'fas fa-chart-area text-primary', NULL, '1', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (2, 0, 2, 'administrator', 'Administrator', NULL, NULL, 'fas fa-chart-area text-primary', NULL, '2', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (3, 2, 2, 'manage-user', 'Manage User', 'acl/user', NULL, '', NULL, '1', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (4, 2, 10, 'manage-menu', 'Manage Menu', 'acl/menu', NULL, '', NULL, '2', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (5, 2, 6, 'manage-role', 'Manage Role', 'acl/role', NULL, '', NULL, '3', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (6, 2, 10, 'manage-authorization', 'Manage Authorization', 'acl/authorization', NULL, '', NULL, '4', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (7, 2, 10, 'manage-module', 'Manage Module', 'acl/module', NULL, '', NULL, '5', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (8, 0, 21, 'request', 'Request', 'request', NULL, 'fas fa-chart-area text-primary', NULL, '3', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (9, 0, 25, 'schedule', 'Schedule', 'schedule', NULL, 'fas fa-chart-area text-primary', NULL, '4', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (10, 0, 29, 'peserta', 'Master Peserta', 'peserta-master', NULL, 'fas fa-chart-area text-primary', NULL, '5', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (11, 0, 33, 'monitoring', 'Monitoring', '', NULL, 'fas fa-chart-area text-primary', NULL, '6', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (12, 11, 33, 'condition', 'Condition', 'monitoring', NULL, '', NULL, '1', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (13, 11, 33, 'test-result', 'Test Result', 'monitoring-test-result', NULL, '', NULL, '2', '2020-12-03 14:59:08', '2020-12-03 14:59:08');
INSERT INTO `menus` VALUES (14, 0, 37, 'master-vendor', 'Vendor Master', 'vendor', NULL, 'fas fa-chart-area text-primary', NULL, '7', '2020-12-03 14:59:08', '2020-12-03 14:59:08');

SET FOREIGN_KEY_CHECKS = 1;
