/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50711
 Source Host           : localhost:3306
 Source Schema         : covid_mnc

 Target Server Type    : MySQL
 Target Server Version : 50711
 File Encoding         : 65001

 Date: 03/12/2020 17:48:24
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for master_vendor
-- ----------------------------
DROP TABLE IF EXISTS `master_vendor`;
CREATE TABLE `master_vendor`  (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `vendor_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `test_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `amount` int(10) UNSIGNED NOT NULL,
  `kuota` int(10) UNSIGNED NOT NULL,
  `created_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `updated_by` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` timestamp(0) NULL DEFAULT NULL,
  `updated_at` timestamp(0) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 13 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of master_vendor
-- ----------------------------
INSERT INTO `master_vendor` VALUES (1, 'RSPP', 'Swab', 100, 100, NULL, NULL, NULL, NULL);
INSERT INTO `master_vendor` VALUES (2, 'RS Mitra Keluarga', 'Swab', 100, 100, NULL, NULL, NULL, NULL);
INSERT INTO `master_vendor` VALUES (3, 'RS Hermina Kemayoran', 'Swab', 100, 100, NULL, NULL, NULL, NULL);
INSERT INTO `master_vendor` VALUES (4, 'Fastlab', 'Swab', 100, 100, NULL, NULL, NULL, NULL);
INSERT INTO `master_vendor` VALUES (5, 'Medika Plaza', 'Swab', 100, 100, NULL, NULL, NULL, NULL);
INSERT INTO `master_vendor` VALUES (6, 'Homecare 24 (HC24)', 'Rapid', 100, 100, NULL, NULL, NULL, NULL);
INSERT INTO `master_vendor` VALUES (7, 'Klinik SehatQ', 'Rapid', 100, 100, NULL, NULL, NULL, NULL);
INSERT INTO `master_vendor` VALUES (8, 'RS Cikini', 'Rapid', 100, 100, NULL, NULL, NULL, NULL);
INSERT INTO `master_vendor` VALUES (9, 'RS Yarsi', 'Rapid', 100, 16, NULL, 'ga_test', NULL, '2020-12-01 13:39:46');
INSERT INTO `master_vendor` VALUES (10, 'Satgas 3TV', 'Rapid', 100, 100, NULL, NULL, NULL, NULL);
INSERT INTO `master_vendor` VALUES (11, 'Pribadi', 'rapid', 999999, 999999, 'admin', 'admin', '2020-12-03 17:23:58', '2020-12-03 17:47:12');
INSERT INTO `master_vendor` VALUES (12, 'Pribadi', 'swab', 999999, 999999, 'admin', NULL, '2020-12-03 17:47:28', '2020-12-03 17:47:28');

SET FOREIGN_KEY_CHECKS = 1;
