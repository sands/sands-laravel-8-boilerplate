/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50711
 Source Host           : localhost:3306
 Source Schema         : covid_mnc

 Target Server Type    : MySQL
 Target Server Version : 50711
 File Encoding         : 65001

 Date: 25/11/2020 19:57:03
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for permission_role
-- ----------------------------
DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE `permission_role`  (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`, `role_id`) USING BTREE,
  INDEX `role_permission`(`role_id`) USING BTREE,
  CONSTRAINT `permission_role` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_permission` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_unicode_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission_role
-- ----------------------------
INSERT INTO `permission_role` VALUES (1, 1);
INSERT INTO `permission_role` VALUES (2, 1);
INSERT INTO `permission_role` VALUES (3, 1);
INSERT INTO `permission_role` VALUES (4, 1);
INSERT INTO `permission_role` VALUES (5, 1);
INSERT INTO `permission_role` VALUES (6, 1);
INSERT INTO `permission_role` VALUES (7, 1);
INSERT INTO `permission_role` VALUES (8, 1);
INSERT INTO `permission_role` VALUES (9, 1);
INSERT INTO `permission_role` VALUES (10, 1);
INSERT INTO `permission_role` VALUES (11, 1);
INSERT INTO `permission_role` VALUES (12, 1);
INSERT INTO `permission_role` VALUES (13, 1);
INSERT INTO `permission_role` VALUES (14, 1);
INSERT INTO `permission_role` VALUES (15, 1);
INSERT INTO `permission_role` VALUES (16, 1);
INSERT INTO `permission_role` VALUES (17, 1);
INSERT INTO `permission_role` VALUES (18, 1);
INSERT INTO `permission_role` VALUES (19, 1);
INSERT INTO `permission_role` VALUES (20, 1);
INSERT INTO `permission_role` VALUES (21, 1);
INSERT INTO `permission_role` VALUES (22, 1);
INSERT INTO `permission_role` VALUES (23, 1);
INSERT INTO `permission_role` VALUES (24, 1);
INSERT INTO `permission_role` VALUES (25, 1);
INSERT INTO `permission_role` VALUES (26, 1);
INSERT INTO `permission_role` VALUES (27, 1);
INSERT INTO `permission_role` VALUES (28, 1);
INSERT INTO `permission_role` VALUES (29, 1);
INSERT INTO `permission_role` VALUES (30, 1);
INSERT INTO `permission_role` VALUES (31, 1);
INSERT INTO `permission_role` VALUES (32, 1);
INSERT INTO `permission_role` VALUES (33, 1);
INSERT INTO `permission_role` VALUES (34, 1);
INSERT INTO `permission_role` VALUES (35, 1);
INSERT INTO `permission_role` VALUES (36, 1);
INSERT INTO `permission_role` VALUES (37, 1);
INSERT INTO `permission_role` VALUES (38, 1);
INSERT INTO `permission_role` VALUES (39, 1);
INSERT INTO `permission_role` VALUES (40, 1);
INSERT INTO `permission_role` VALUES (1, 2);
INSERT INTO `permission_role` VALUES (21, 2);
INSERT INTO `permission_role` VALUES (25, 2);
INSERT INTO `permission_role` VALUES (29, 2);
INSERT INTO `permission_role` VALUES (33, 2);
INSERT INTO `permission_role` VALUES (37, 2);
INSERT INTO `permission_role` VALUES (1, 3);
INSERT INTO `permission_role` VALUES (17, 3);
INSERT INTO `permission_role` VALUES (18, 3);
INSERT INTO `permission_role` VALUES (19, 3);
INSERT INTO `permission_role` VALUES (21, 3);
INSERT INTO `permission_role` VALUES (22, 3);
INSERT INTO `permission_role` VALUES (23, 3);
INSERT INTO `permission_role` VALUES (24, 3);
INSERT INTO `permission_role` VALUES (25, 3);
INSERT INTO `permission_role` VALUES (26, 3);
INSERT INTO `permission_role` VALUES (27, 3);
INSERT INTO `permission_role` VALUES (28, 3);
INSERT INTO `permission_role` VALUES (29, 3);
INSERT INTO `permission_role` VALUES (30, 3);
INSERT INTO `permission_role` VALUES (31, 3);
INSERT INTO `permission_role` VALUES (33, 3);
INSERT INTO `permission_role` VALUES (34, 3);
INSERT INTO `permission_role` VALUES (35, 3);
INSERT INTO `permission_role` VALUES (36, 3);
INSERT INTO `permission_role` VALUES (37, 3);
INSERT INTO `permission_role` VALUES (38, 3);
INSERT INTO `permission_role` VALUES (39, 3);
INSERT INTO `permission_role` VALUES (40, 3);
INSERT INTO `permission_role` VALUES (1, 4);
INSERT INTO `permission_role` VALUES (17, 4);
INSERT INTO `permission_role` VALUES (21, 4);
INSERT INTO `permission_role` VALUES (22, 4);
INSERT INTO `permission_role` VALUES (23, 4);
INSERT INTO `permission_role` VALUES (24, 4);
INSERT INTO `permission_role` VALUES (25, 4);
INSERT INTO `permission_role` VALUES (26, 4);
INSERT INTO `permission_role` VALUES (27, 4);
INSERT INTO `permission_role` VALUES (28, 4);
INSERT INTO `permission_role` VALUES (29, 4);
INSERT INTO `permission_role` VALUES (30, 4);
INSERT INTO `permission_role` VALUES (31, 4);
INSERT INTO `permission_role` VALUES (37, 4);
INSERT INTO `permission_role` VALUES (38, 4);
INSERT INTO `permission_role` VALUES (39, 4);
INSERT INTO `permission_role` VALUES (1, 5);
INSERT INTO `permission_role` VALUES (25, 5);
INSERT INTO `permission_role` VALUES (26, 5);
INSERT INTO `permission_role` VALUES (27, 5);
INSERT INTO `permission_role` VALUES (28, 5);

SET FOREIGN_KEY_CHECKS = 1;
