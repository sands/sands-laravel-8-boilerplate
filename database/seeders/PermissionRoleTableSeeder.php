<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Module;
use App\Models\Role;
use App\Models\Permission;
use App\Menu;
use Illuminate\Support\Facades\DB;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /***********SETTING ROLE*********/
        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'super-admin',
            'display_name' => 'Super Admin',
            'description' => 'All Access',
        ]);
        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'head',
            'display_name' => 'Section Head',
            'description' => 'Section Head',
        ]);
        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'operator',
            'display_name' => 'Operator',
            'description' => 'Operator',
        ]);
        DB::table('roles')->insert([
            'id' => 4,
            'name' => 'user',
            'display_name' => 'User',
            'description' => 'User',
        ]);


        /***********ATTACH ROLE PERMISSION*********/
        $permissions = Permission::all();
        $superAdmin = Role::where('name','super-admin')->first();
        $head = Role::where('name','head')->first();
        $operator = Role::where('name','operator')->first();
        $user = Role::where('name','user')->first();

        foreach($permissions as $permission){
            //SUPER ADMIN
            $superAdmin->attachPermission($permission);

            switch($permission->name){
                case 'show-dashboard':
                    $head->attachPermission($permission);
                    $operator->attachPermission($permission);
                    $user->attachPermission($permission);
                    break;
            }
        }

    }
}
