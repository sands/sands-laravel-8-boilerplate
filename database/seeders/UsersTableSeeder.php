<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //CREATE USERS
        DB::table('users')->insert([
            'id'=>1,
            'fullname' => 'Administrator',
            'username' => 'admin',
            'email' => 'admin@mncgroup.com',
            'password' => bcrypt('admin'),
        ]);

        //ATTACH USER_ROLE
        $user = User::where('email','admin@mncgroup.com')->first();
        $superAdmin = Role::where('name','super-admin')->first();
        $user->attachRole($superAdmin);
    }
}