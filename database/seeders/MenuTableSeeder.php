<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenuTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        /***********SETTING MENU*********/
        DB::table('menus')->insert([
            'name' => 'dashboard',
            'permission_id' => 1,
            'display_name' => 'Dashboard',
            'description' => 'dashboard',
            'parent_id' => 0,
            'icon' => 'fas fa-chart-area text-primary',
            'icon_child' => '',
            'seq' => '1',
            'url' => 'home'
        ]);
        DB::table('menus')->insert([
            'name' => 'administrator',
            'permission_id' => 2,
            'display_name' => 'Administrator',
            'description' => 'Administrator',
            'parent_id' => 0,
            'icon' => 'fas fa-chart-area text-primary',
            'icon_child' => '',
            'seq' => '2'
        ]);
        DB::table('menus')->insert([
            'name' => 'manage-user',
            'permission_id' => 2,
            'display_name' => 'Manage User',
            'description' => 'Manage User',
            'parent_id' => 2,
            'icon' => '',
            'icon_child' => '',
            'seq' => '1',
            'url' => 'acl/user'
        ]);
        DB::table('menus')->insert([
            'name' => 'manage-menu',
            'permission_id' => 10,
            'display_name' => 'Manage Menu',
            'description' => 'Manage Menu',
            'parent_id' => 2,
            'icon' => '',
            'icon_child' => '',
            'seq' => '2',
            'url' => 'acl/menu'
        ]);
        DB::table('menus')->insert([
            'name' => 'manage-role',
            'permission_id' => 6,
            'display_name' => 'Manage Role',
            'description' => 'Manage Role',
            'parent_id' => 2,
            'icon' => '',
            'icon_child' => '',
            'seq' => '3',
            'url' => 'acl/role'
        ]);
        DB::table('menus')->insert([
            'name' => 'manage-authorization',
            'permission_id' => 10,
            'display_name' => 'Manage Authorization',
            'description' => 'Manage Authorization',
            'parent_id' => 2,
            'icon' => '',
            'icon_child' => '',
            'seq' => '4',
            'url' => 'acl/authorization'
        ]);
        DB::table('menus')->insert([
            'name' => 'manage-module',
            'permission_id' => 10,
            'display_name' => 'Manage Module',
            'description' => 'Manage Module',
            'parent_id' => 2,
            'icon' => '',
            'icon_child' => '',
            'seq' => '5',
            'url' => 'acl/module'
        ]);


    }
}
