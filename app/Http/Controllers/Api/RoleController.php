<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Role;
use Validator;
use App\Http\Library\Serializer;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeCollection(true,$message,$roles);
        return response()->json($resource,$statusCode);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->getValidator('create',$request);
        
        if($validator->fails()){
            $messages = implode(',',array_column($validator->messages()->toArray(),0));
            
            $resource = Serializer::serializeItem(true,$messages);
            $statusCode = 422;
            return response()->json($resource,$statusCode);
        }else{
            $role = new Role;
            $max_id = Role::max('id');
            if($max_id != null){
                $role->id = $max_id+1;
            }else{
                $role->id = 1;
            }
            $role->name = $request->name;
            $role->display_name = $request->display_name;
            $role->description = $request->description;
            $role->save();
            $message = "Success Add Role";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true,$message,$role);
            return response()->json($resource,$statusCode);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $role = Role::find($id);
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(false,$message,$role);

        return response()->json($resource,$statusCode);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->getValidator('update',$request);
        
        if($validator->fails()){
            $messages = implode(',',array_column($validator->messages()->toArray(),0));
            
            $resource = Serializer::serializeItem(false,$messages);
            $statusCode = 422;
            return response()->json($resource,$statusCode);
        }else{
            $role = Role::find($id);
            if($role === null){
                $message = "Role with id: ".$id.", does not exist";
                $statusCode = 500;
                $resource = Serializer::serializeItem(false,$message);
                return response()->json($resource,$statusCode);
            }
            if($request->name != null && $request->name != '')
            $role->name = $request->name;
            if($request->display_name != null && $request->display_name != '')
            $role->display_name = $request->display_name;
            $role->description = $request->description;
            $role->save();
            $message = "success";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true,$message);
            return response()->json($resource,$statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $role = Role::find($id);
        if($role === null){
            $message = "Role with id: ".$id.", does not exist";
            $statusCode = 500;
            $resource = Serializer::serializeItem(false,$message);
            return response()->json($resource,$statusCode);
        }
        $role->delete();
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(true,$message);
        return response()->json($resource,$statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function getValidator($method, Request $request)
    {
        if($method == 'create'){
            return Validator::make($request->all(),[
                'name' => 'required|string|max:255|unique:roles',
                'display_name' => 'required|string|max:255',
                'description' => 'string|max:255',
            ]);
        }else if ($method =='update'){
            return Validator::make($request->all(),[
                'name' => 'string|max:255|unique:roles',
                'display_name' => 'string|max:255',
                'description' => 'string|max:255',
            ]);
        }
        
    }
}
