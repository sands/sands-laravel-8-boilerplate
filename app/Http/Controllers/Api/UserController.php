<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Library\Serializer;
use App\Http\Library\TableHelper;
use App\models\Role;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::with('roles')
            ->with('unit')
            ->get()
            ->sortBy('fullname');

        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeCollection(true, $message, $users);

        return response()->json($resource, $statusCode);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->getValidator('create', $request);
        $statusCode = 422;
        $statusResp = false;

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));

            $resource = Serializer::serializeItem($statusResp, $messages);
            return response()->json($resource, $statusCode);
        } else {
            DB::beginTransaction();
            try {
                $messages =  "Gagal Simpan Data";
                $user = new User;
                $user->fullname = $request->name;
                $user->email = $request->email;
                $user->email_receiver = $request->email;
                $user->username = $request->username;
                $user->status = $request->status;
                $user->unit_id = $request->unit_id;
                $user->password = bcrypt(User::DEFAULT_PASS);

                if ($user->save()) {
                    $user->attachRole(Role::find($request->role));
                    $messages = "Berhasil Simpan Data";
                    DB::commit();
                    $statusCode = Response::HTTP_OK;
                    $statusResp = true;
                }
            } catch (\Exception $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            } catch (\Throwable $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            }

            $resource = Serializer::serializeItem($statusResp, $messages, User::where('id', $user->id)->with('roles')->first());
            return response()->json($resource, $statusCode);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::with('roles')->where('id', $id)->first();
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(true, $message, $user);

        return response()->json($resource, $statusCode);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->getValidator('update', $request, $id);

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));

            $resource = Serializer::serializeItem(false, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        } else {
            $user = User::find($id);
            if ($user === null) {
                $message = "User with id: " . $id . ", does not exist";
                $statusCode = 500;
                $resource = Serializer::serializeItem(false, $message);
                return response()->json($resource, $statusCode);
            }
            if ($request->name != null && $request->name != '')
                $user->fullname = $request->name;
            if ($request->username != null && $request->username != '')
                $user->username = $request->username;
            if ($request->email != null && $request->email != '')
                $user->email = $request->email;
            if ($request->password != null && $request->password != '')
                $user->password = bcrypt($request->password);
            if ($request->status != null && $request->status != '')
                $user->status = $request->status;
            
            $user->unit_id = !empty($request->unit_id) ? $request->unit_id : null;

            $user->detachRoles($user->roles);
            $user->attachRole(Role::find($request->role));
            $user->save();
            $message = "Success";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true, $message);
            return response()->json($resource, $statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if ($user === null) {
            $message = "User with id: " . $id . ", does not exist";
            $statusCode = 500;
            $resource = Serializer::serializeItem(false, $message);
            return response()->json($resource, $statusCode);
        }
        $user->delete();
        $message = "Delete Success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(true, $message);
        return response()->json($resource, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getTableData(Request $request, $limit = 10)
    {
        $query = User::with('roles')->with('unit');
        //attach where clause
        $wheres = TableHelper::getKeys($request->all(), 'where-');

        foreach ($wheres as $where) {
            $column = array_search($where, $wheres);
            // $query = $query->where($column, "like", "%" . strtoupper($where) . "%");
        }

        //attach orderBy clause
        $orderBys = TableHelper::getKeys($request->all(), 'order-');
        foreach ($orderBys as $orderBy) {
            $column = array_search($orderBy, $orderBys);
            $query = $query->orderBy($column, $orderBy);
        }

        if (!empty($request->search)) {
            $search_q = $request->search;
            $query->where(function ($query) use ($search_q) {
                $query->where('fullname', 'LIKE', '%' . $search_q . '%')
                    ->orWhere('username', 'LIKE', '%' . $search_q . '%')
                    ->orWhere('email', 'LIKE', '%' . $search_q . '%')
                    ->orWhere('status', 'LIKE', '%' . $search_q . '%');
            });
        }

        $data = $query->paginate($limit);
        $count = $query->count();

        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeCollection(true, $message, $data, $count);
        return response()->json($resource, $statusCode);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function getValidator($method, Request $request, $id = null)
    {
        if ($method == 'create') {
            return Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users',
                'role' => 'required|integer|exists:roles,id',
                'status' => 'required'
            ]);
        } else if ($method == 'update') {
            return Validator::make($request->all(), [
                'name' => 'required|string|max:255',
                'username' => 'required|string|max:255',
                'email' => 'required|string|email|max:255|unique:users,email,' . $id,
                'role' => 'required|integer|exists:roles,id',
                'status' => 'required'
            ]);
        }
    }
}
