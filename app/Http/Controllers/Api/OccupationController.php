<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Library\Serializer;
use App\Http\Library\TableHelper;
use App\Models\MasterOccupation;
use JWTAuth;

class OccupationController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function getValidator($method, Request $request, $id = null)
    {
        if ($method == 'create') {
            return Validator::make($request->all(), [
                'name' => 'required|string|max:255|unique:master_occupation',
                'desc' => 'string|max:255',
            ]);
        } else if ($method == 'update') {
            return Validator::make($request->all(), [
                'name' => 'required|string|max:255|unique:master_occupation,name,' . $id . ',id',
                'desc' => 'string|max:255',
            ]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $is_superadmin = $user->hasRole(['super-admin']);
        $unit_id = $user->unit_id;

        $occups = MasterOccupation::all();

        if(!$is_superadmin){
            $valid_occups = ['operator', 'crew chief', 'koordinator', 'kordinator'];
            $occups = $occups->filter(function($v, $k) use ($valid_occups){
                $name = strtolower($v->name);
                $is_valid = false;
                foreach ($valid_occups as $key => $value) {
                    if(strpos($name, $value) !== FALSE){
                        $is_valid = true;
                    }
                }

                if($is_valid == TRUE){
                    return $v;
                }
            });
        }

        $message = "Success";
        $statusCode = 200;
        $resource = Serializer::serializeCollection(true, $message, $occups);

        return response()->json($resource, $statusCode);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->getValidator('create', $request);

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));
            $resource = Serializer::serializeItem(true, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        } else {
            $occup = new MasterOccupation();
            $occup->name = $request->name;
            $occup->status = $request->status;
            $occup->description = $request->description;
            $occup->save();
            $message = "Success Add Data";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true, $message, $occup);
            return response()->json($resource, $statusCode);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $occup = MasterOccupation::find($id);
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(false, $message, $occup);

        return response()->json($resource, $statusCode);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->getValidator('update', $request, $id);

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));

            $resource = Serializer::serializeItem(false, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        } else {
            $occup = MasterOccupation::find($id);
            if ($occup === null) {
                $message = "Occupation with id: " . $id . ", does not exist";
                $statusCode = 500;
                $resource = Serializer::serializeItem(false, $message);
                return response()->json($resource, $statusCode);
            }

            $occup->name = $request->name;
            $occup->status = $request->status;
            $occup->description = $request->description;
            $occup->save();

            $message = "Success";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true, $message);
            return response()->json($resource, $statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $occup = MasterOccupation::find($id);
        if ($occup === null) {
            $message = "Occupation with id: " . $id . ", does not exist";
            $statusCode = 500;
            $resource = Serializer::serializeItem(false, $message);
            return response()->json($resource, $statusCode);
        }
        $occup->delete();
        $message = "Delete Success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(true, $message);
        return response()->json($resource, $statusCode);
    }
}
