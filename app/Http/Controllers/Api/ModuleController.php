<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Module;
use App\models\Role;
use Validator;
use App\Http\Library\Serializer;
use App\Models\Permission;
use App\Models\PermissionRole;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class ModuleController extends Controller
{
    private function getValidator($method, Request $request, $id = null)
    {
        if ($method == 'moduleStore') {
            return \Illuminate\Support\Facades\Validator::make($request->all(), [
                'name' => 'required',
                'display_name' => 'required',
                'status' => 'required',
                'permissions' => 'required'
            ]);
        } elseif ($method == 'moduleUpdate') {
            return \Illuminate\Support\Facades\Validator::make($request->all(), [
                'name' => 'required',
                'display_name' => 'required',
                'permissions' => 'required',
                'status' => 'required'
            ]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modules = Module::with('permissions')->get();

        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeCollection(true, $message, $modules);

        return response()->json($resource, $statusCode);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $module = Module::find($id);
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(true, $message, $module);

        return response()->json($resource, $statusCode);
    }

    public function store(Request $request)
    {
        $validator = $this->getValidator('moduleStore', $request);

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));
            return redirect()->back()->with('msg', $messages);
        } else {
            DB::beginTransaction();
            $statusCode = Response::HTTP_BAD_REQUEST;
            $statusResp = false;

            try {
                $permissions = json_decode($request->permissions);
                if (count($permissions) > 0) {
                    $module = new Module();
                    $module->name = strtolower(str_replace(' ', '',  $request->name));
                    $module->display_name = $request->display_name;
                    $module->description = !empty($request->description) ?  $request->description : '';
                    $module->save();

                    foreach ($permissions as $k => $permission) {
                        $permission_name = $permission . '-' . $module->name;
                        $display_name = ucwords($permission . ' ' . $module->name);

                        $module_id = $module->id;
                        DB::table('permissions')->insert([
                            'name' => $permission_name,
                            'display_name' => $display_name,
                            'description' => $display_name,
                            'module_id' => $module_id,
                        ]);
                    }

                    $statusCode = Response::HTTP_OK;
                    $statusResp = true;
                    $messages = "Data Berhasil Disimpan";
                } else {
                    $messages = "Data Gagal Disimpan, Permission Belum Dipilih";
                }

                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $messages = $e;
                // throw $e;
            } catch (\Throwable $e) {
                DB::rollback();
                $messages = $e;
                // throw $e;
            }

            $resource = Serializer::serializeItem($statusResp, $messages, null);
            return response()->json($resource, $statusCode);
        }
    }

    public function update(Request $request, $id = null)
    {
        $validator = $this->getValidator('moduleUpdate', $request);
        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));
            return redirect()->back()->with('msg', $messages);
        } else {
            DB::beginTransaction();
            $statusCode = Response::HTTP_BAD_REQUEST;
            $statusResp = false;

            try {
                $permissions = json_decode($request->permissions);

                if (count($permissions) > 0) {
                    $module = Module::find($id);
                    $module->name = strtolower(str_replace(' ', '',  $request->name));
                    $module->display_name = $request->display_name;
                    $module->status = $request->status;
                    $module->description = !empty($request->description) ?  $request->description : '';
                    $module->save();

                    //delete permission and permission role existing
                    $exist_permission = $module->permissions;
                    foreach ($exist_permission as $perm) {
                        $perm_exist_roles_delete = PermissionRole::where('permission_id', $perm->id)->delete();
                    }
                    $perm_exist_delete = Permission::where('module_id', $module->id)->delete();

                    foreach ($permissions as $k => $permission) {
                        $permission_name = $permission . '-' . $module->name;
                        $display_name = ucwords($permission . ' ' . $module->name);

                        $module_id = $module->id;
                        DB::table('permissions')->insert([
                            'name' => $permission_name,
                            'display_name' => $display_name,
                            'description' => $display_name,
                            'module_id' => $module_id,
                        ]);
                    }
                    $statusCode = Response::HTTP_OK;
                    $statusResp = true;
                    $messages = "Data Berhasil Disimpan, Harap Memberikan Auth Ulang Pada Module yang baru diubah dengan menuju menu Manage Authorization";
                } else {
                    $messages = "Data Gagal Disimpan, Permission Belum Dipilih";
                }
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            } catch (\Throwable $e) {
                DB::rollback();
                $messages = $e;
                //throw $e;
            }
            $resource = Serializer::serializeItem($statusResp, $messages, null);
            return response()->json($resource, $statusCode);
        }
    }

    public function destroy($id)
    {
        $data = Module::find($id);
        if($data === null){
            $message = "Module with id: ".$id.", does not exist";
            $statusCode = 500;
            $resource = Serializer::serializeItem(false,$message);
            return response()->json($resource,$statusCode);
        }

        //delete permission and permission role existing
        $exist_permission = $data->permissions;
        foreach ($exist_permission as $perm) {
            $perm_exist_roles_delete = PermissionRole::where('permission_id', $perm->id)->delete();
        }
        $perm_exist_delete = Permission::where('module_id', $data->id)->delete();

        $data->delete();

        $message = "Delete Success";
        $statusCode = Response::HTTP_OK;
        $resource = Serializer::serializeItem(true,$message);
        return response()->json($resource,$statusCode);
    }

}
