<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Library\Serializer;
use App\Http\Library\TableHelper;
use App\Models\Department;

class DepartmentController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function getValidator($method, Request $request, $id = null)
    {
        if ($method == 'create') {
            return Validator::make($request->all(), [
                'name' => 'required|string|max:255|unique:master_department',
                'status' => 'required',
            ]);
        } else if ($method == 'update') {
            return Validator::make($request->all(), [
                'name' => 'required|string|max:255|unique:master_department,name,' . $id . ',id',
                'status' => 'required',
            ]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $departments = Department::all();
        $message = "Success Get Data";
        $statusCode = 200;
        $resource = Serializer::serializeCollection(true, $message, $departments);

        return response()->json($resource, $statusCode);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->getValidator('create', $request);

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));
            $resource = Serializer::serializeItem(true, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        } else {
            $department = new Department();
            $department->name = $request->name;
            $department->status = $request->status;
            $department->save();
            $message = "Success Add Data";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true, $message, $department);
            return response()->json($resource, $statusCode);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unit = Department::find($id);
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(false, $message, $unit);

        return response()->json($resource, $statusCode);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->getValidator('update', $request, $id);

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));

            $resource = Serializer::serializeItem(false, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        } else {
            $department = Department::find($id);
            
            if ($department === null) {
                $message = "Department with id: " . $id . ", does not exist";
                $statusCode = 500;
                $resource = Serializer::serializeItem(false, $message);
                return response()->json($resource, $statusCode);
            }

            $department->name = $request->name;
            $department->status = $request->status;
            $department->save();

            $message = "Update Data Success";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true, $message);
            return response()->json($resource, $statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $department = Department::find($id);
        if ($department === null) {
            $message = "Department with id: " . $id . ", does not exist";
            $statusCode = 500;
            $resource = Serializer::serializeItem(false, $message);
            return response()->json($resource, $statusCode);
        }
        $department->delete();
        $message = "Delete data Success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(true, $message);
        return response()->json($resource, $statusCode);
    }
}
