<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTFactory;
use JWTAuth;
use App\Http\Library\Serializer;
use App\User;
use App\models\Role;
use App\Captcha;
use App\LogAuthUser;
use App\Models\User as ModelsUser;
use App\Notification;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuthenticateController extends Controller
{
    public function authenticate(Request $request)
    {
        $email = $request->input('email');
        $email = str_replace("@mncgroup.com", "", $email);
        $password = $request->input('password');

        $credentials = $request->only('email', 'password');

        //valid credential
        $validator = Validator::make($credentials, [
            'email' => 'required',
            'password' => 'required|string|min:5|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            $message = $validator->messages();
            $statusCode = Response::HTTP_BAD_REQUEST;
            $resource = Serializer::serializeItem(false, $message, null);
            return response()->json($resource, $statusCode);
        }

        //LDAP Login
        $check_ldap = self::check_ldap($email, $password);
        if($check_ldap){
            $user = ModelsUser::where('username', $email)->first();
            $token = JWTAuth::fromUser($user);
            return response()->json(compact('token'));
        }

        //Non LDAP Login
        $cred_username = [
            'username' => $email,
            'password' => $password
        ];
        $check_username = Auth::attempt($cred_username);

        $cred_email = [
            'email' => $email,
            'password' => $password
        ];
        $check_email = Auth::attempt($cred_email);

        if($check_email || $check_username){
            $user = ModelsUser::whereRaw('email = "'.$email.'" OR username = "'.$email.'"')
                ->where('status', ModelsUser::STATUS_ACTIVE)
                ->first();
            $token = JWTAuth::fromUser($user);
            return response()->json(compact('token'));
        }else{
            $message = "Data tidak ditemukan";
            $statusCode = Response::HTTP_BAD_REQUEST;
            $resource = Serializer::serializeItem(false, $message, null);
            return response()->json($resource, $statusCode);
        }
        
    }

    private function check_ldap($username, $password)
    {
        // return true;
        $ldap_ip = '172.18.8.10';
        $ldap_port = 389;
        $ldap_domain = "@mncgroup.com";

        $ds = ldap_connect($ldap_ip, $ldap_port) or die("Could not connect");

        if ($ds)
        {
            try
            {
                $usercode = $username . $ldap_domain;
                $ldapbind = @ldap_bind($ds, $usercode, $password);
                // $ldap_dn = 'OU=MNCMedia,DC=mncgroup,DC=com';
                // $results = @ldap_search($ds, $ldap_dn, "(mail=$usercode)");
                // $entries = @ldap_get_entries($ds, $results);

                if (@$ldapbind)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception $e)
            {
                return false;
            }
        }
    }

    public function authUser()
    {
        $user = JWTAuth::parseToken()->authenticate();

        //set role name
        $user->role_name = $user->roles->first()->name;

        $message = "success";
        $statusCode = Response::HTTP_OK;
        $resource = Serializer::serializeItem(true, $message, $user);
        return response()->json($resource, $statusCode);
    }


    public function invalidate()
    {
        JWTAuth::parseToken()->invalidate();
        //JWTAuth::invalidate(JWTAuth::getToken());
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(true, $message);
        return response()->json($resource, $statusCode);
    }
}
