<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTFactory;
use JWTAuth;
use App\Http\Library\Serializer;
use App\User;
use App\models\Role;
use App\Captcha;
use App\LogAuthUser;
use App\Models\Menu;
use App\Models\MenuRole;
use App\Models\User as ModelsUser;
use App\Notification;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class MenuController extends Controller
{
    public $menu_role;

    private function getValidator($method, Request $request, $id = null)
    {
        if ($method == 'menusStore') {
            return \Illuminate\Support\Facades\Validator::make($request->all(), [
                'menus' => 'required',
            ]);
        } else if ($method == 'toggleRoleMenu') {
            return \Illuminate\Support\Facades\Validator::make($request->all(), [
                'id' => 'required',
                'role_id' => 'required',
                'is_check' => 'required',
            ]);
        }
    }

    function __construct(MenuRole $menu_role)
    {
        $this->menu_role = $menu_role;
    }

    public function menus()
    {
        $user = JWTAuth::parseToken()->authenticate();
        $role = $user->roles->first();
        $perms = $role->perms->pluck('id');
        $menus = Menu::whereIn('permission_id', $perms)
            ->orderBy('parent_id', 'ASC')
            ->orderBy('seq', 'ASC')
            ->get();

        $menus_used = [];
        foreach ($menus as $menu_data) {
            if ($menu_data->parent_id != 0) {
                $menu_parent = Menu::where('id', $menu_data->parent_id)->first();
                $seq_parent = (int) $menu_parent->seq;

                $data_child = [
                    'id' => $menu_data->id,
                    'name' => $menu_data->name,
                    'parent_seq' => $seq_parent,
                    'parent_id' => $menu_data->parent_id,
                    'display_name' => $menu_data->display_name,
                    'icon' => $menu_data->icon,
                    'status_active' => $menu_data->status_active,
                    'type' => $menu_data->type,
                    'url' => $menu_data->url,
                    'seq' => (int) $menu_data->seq,
                    'permission_id' => $menu_data->permission_id
                ];

                $menus_used[$seq_parent]['children'][$menu_data->seq] = $data_child;
            } else {
                $menus_used[$menu_data->seq] = [
                    'id' => $menu_data->id,
                    'name' => $menu_data->name,
                    'display_name' => $menu_data->display_name,
                    'icon' => $menu_data->icon,
                    'status_active' => $menu_data->status_active,
                    'type' => $menu_data->type,
                    'url' => $menu_data->url,
                    'seq' => (int) $menu_data->seq,
                    'permission_id' => $menu_data->permission_id
                ];
            }
        }

        $message = "success";
        $statusCode = Response::HTTP_OK;
        $resource = Serializer::serializeItem(true, $message, $menus_used);
        return response()->json($resource, $statusCode);
    }

    public function menusByRole($id = null)
    {
        $menus_by_role = MenuRole::with('menu')->where('role_id', $id)->get()
            ->pluck('menu')
            ->sortBy('parent_id')
            ->sortBy('seq');
        $menus_used = [];
        foreach ($menus_by_role as $menu_data) {
            //if child menu
            if ($menu_data->parent_id != 0) {
                $menu_parent = Menu::where('id', $menu_data->parent_id)->first();
                $seq_parent = (int) $menu_parent->seq;

                $data_child = [
                    'id' => $menu_data->id,
                    'name' => $menu_data->name,
                    'parent_seq' => $seq_parent,
                    'parent_id' => $menu_data->parent_id,
                    'display_name' => $menu_data->display_name,
                    'icon' => $menu_data->icon,
                    'status_active' => $menu_data->status_active,
                    'type' => $menu_data->type,
                    'url' => $menu_data->url,
                    'seq' => (int) $menu_data->seq,
                    'permission_id' => $menu_data->permission_id
                ];

                $menus_used[$seq_parent]['children'][$menu_data->seq] = $data_child;
            } else {
                if (!empty($menus_used[$menu_data->seq]['children'])) {
                    $menus_used[$menu_data->seq] = [
                        'id' => $menu_data->id,
                        'name' => $menu_data->name,
                        'display_name' => $menu_data->display_name,
                        'icon' => $menu_data->icon,
                        'status_active' => $menu_data->status_active,
                        'type' => $menu_data->type,
                        'url' => $menu_data->url,
                        'seq' => (int) $menu_data->seq,
                        'permission_id' => $menu_data->permission_id,
                        'children' => $menus_used[$menu_data->seq]['children']
                    ];
                } else {
                    $menus_used[$menu_data->seq] = [
                        'id' => $menu_data->id,
                        'name' => $menu_data->name,
                        'display_name' => $menu_data->display_name,
                        'icon' => $menu_data->icon,
                        'status_active' => $menu_data->status_active,
                        'type' => $menu_data->type,
                        'url' => $menu_data->url,
                        'seq' => (int) $menu_data->seq,
                        'permission_id' => $menu_data->permission_id
                    ];
                }
            }
        }

        $message = "success";
        $statusCode = Response::HTTP_OK;
        $resource = Serializer::serializeItem(true, $message, $menus_used);
        return response()->json($resource, $statusCode);
    }

    public function menuRole($id = null)
    {
        $menu_role = MenuRole::where('menu_id', $id)->get();
        $message = "success";
        $statusCode = Response::HTTP_OK;
        $resource = Serializer::serializeItem(true, $message, $menu_role);
        return response()->json($resource, $statusCode);
    }

    public function menusStore(Request $request)
    {
        $validator = $this->getValidator('menusStore', $request);

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));

            $resource = Serializer::serializeItem(false, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        }

        $messages = "Data Menu tidak terdapat pada request";
        $menu_json = $request->menus;
        $menus = json_decode($menu_json);
        if (count((array) $menus) > 0) {
            $messages = "Save Data Success";
            $seq = 1;

            DB::beginTransaction();

            DB::table('menus')->truncate();

            foreach ($menus as $menu) {
                try {
                    $menu_new = new Menu();
                    $menu_new->name = $menu->name;
                    $menu_new->permission_id = $menu->permission_id;
                    $menu_new->parent_id = 0;
                    $menu_new->display_name = $menu->display_name;
                    $menu_new->icon = $menu->icon;
                    $menu_new->url = $menu->url;
                    $menu_new->type = $menu->type;
                    $menu_new->status_active = $menu->status_active;
                    $menu_new->seq = $seq;
                    $menu_new->save();

                    //save menu children
                    if (!empty($menu->children)) {
                        $seq_child = 1;
                        foreach ($menu->children as $child) {
                            $menu_child_new = new Menu();
                            $menu_child_new->parent_id = $menu_new->id;
                            $menu_child_new->seq = $seq_child;
                            $menu_child_new->permission_id = $child->permission_id;
                            $menu_child_new->name = $child->name;
                            $menu_child_new->display_name = $child->display_name;
                            $menu_child_new->icon = $child->icon;
                            $menu_child_new->type = $child->type;
                            $menu_child_new->status_active = $child->status_active;
                            $menu_child_new->url = $child->url;
                            $menu_child_new->save();
                            $seq_child++;
                        }
                    }

                    $seq++;
                    DB::commit();
                } catch (\Exception $e) {
                    DB::rollback();
                    $messages = $e;
                    //throw $e;
                } catch (\Throwable $e) {
                    DB::rollback();
                    $messages = $e;
                    //throw $e;
                }
            }
        }

        $resource = Serializer::serializeItem(true, $messages);
        $statusCode = Response::HTTP_OK;
        return response()->json($resource, $statusCode);
    }

    public function toggleRoleMenu(Request $request)
    {
        $validator = $this->getValidator('toggleRoleMenu', $request);
        $statusResp = false;
        $statusCode = Response::HTTP_BAD_REQUEST;

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));

            $resource = Serializer::serializeItem(false, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        }

        $id = $request->id;
        $role_id = $request->role_id;
        $is_check = $request->is_check;

        DB::beginTransaction();
        try {
            if ($is_check == "true") {
                $this->menu_role->attachRoleMenu($id, $role_id);
            } else if ($is_check == "false") {
                $this->menu_role->detachRoleMenu($id, $role_id);
            }

            $statusCode = Response::HTTP_OK;
            $statusResp = true;
            $messages = "Data Berhasil Diproses";

            DB::commit();
        } catch (\Exception $e) {
            DB::rollback();
            $messages = $e->getMessage();
            // throw $e;
        } catch (\Throwable $e) {
            DB::rollback();
            $messages = $e->getMessage();
            // throw $e;
        }

        $resource = Serializer::serializeItem($statusResp, $messages);
        return response()->json($resource, $statusCode);
    }
}
