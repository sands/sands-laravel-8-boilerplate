<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Library\Serializer;
use App\Http\Library\TableHelper;
use App\Models\MasterDeviationType;
use App\Models\UnitUserAccess;
use App\Models\User;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class UnitUserAccessController extends Controller
{
    private function getValidator($method, Request $request, $id = null)
    {
        if ($method == 'updateAccess') {
            return Validator::make($request->all(), [
                'unit_id' => 'required',
                'user_id' => 'required',
                'is_check' => 'required',
            ]);
        }
    }


    public function getTableData(Request $request, $limit = 10)
    {
        $query = User::with('roles')->with('unit')->with('occupation')->with('department')->with('unit_user_access');
        //attach where clause
        $wheres = TableHelper::getKeys($request->all(), 'where-');

        foreach ($wheres as $where) {
            $column = array_search($where, $wheres);
            // $query = $query->where($column, "like", "%" . strtoupper($where) . "%");
        }

        //attach orderBy clause
        $orderBys = TableHelper::getKeys($request->all(), 'order-');
        foreach ($orderBys as $orderBy) {
            $column = array_search($orderBy, $orderBys);
            $query = $query->orderBy($column, $orderBy);
        }

        if (!empty($request->search)) {
            $search_q = $request->search;
            $query->where(function ($query) use ($search_q) {
                $query->where('fullname', 'LIKE', '%' . $search_q . '%')
                    ->orWhere('username', 'LIKE', '%' . $search_q . '%')
                    ->orWhere('email', 'LIKE', '%' . $search_q . '%')
                    ->orWhere('status', 'LIKE', '%' . $search_q . '%');
            });
        }

        $data = $query->paginate($limit);
        $count = $query->count();

        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeCollection(true, $message, $data, $count);
        return response()->json($resource, $statusCode);
    }

    public function updateAccess(Request $request)
    {
        $validator = $this->getValidator('updateAccess', $request);

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));
            $resource = Serializer::serializeItem(true, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        } else {
            DB::beginTransaction();
            $statusCode = Response::HTTP_BAD_REQUEST;
            $statusResp = false;

            try {
                $is_check = $request->is_check;
                $unit_id = $request->unit_id;
                $user_id = $request->user_id;
                if ($is_check == "true") {
                    $access = new UnitUserAccess();
                    $access->unit_id = $unit_id;
                    $access->user_id = $user_id;
                    $access->save();
                } else {
                    $access = UnitUserAccess::where('unit_id', $unit_id)->where('user_id', $user_id);
                    $access->delete();
                }

                $statusCode = Response::HTTP_OK;
                $statusResp = true;
                $messages = "Success Update Data";
                DB::commit();
            } catch (\Exception $e) {
                DB::rollback();
                $messages = $e->getMessage();
                // throw $e;
            } catch (\Throwable $e) {
                DB::rollback();
                $messages = $e->getMessage();
                // throw $e;
            }

            $resource = Serializer::serializeItem($statusResp, $messages, null);
            return response()->json($resource, $statusCode);
        }
    }
}
