<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Validator;
use App\Http\Library\Serializer;
use App\Http\Library\TableHelper;
use App\Models\MasterUnit;
use App\UnitUser;

class UnitController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function getValidator($method, Request $request, $id = null)
    {
        if ($method == 'create') {
            return Validator::make($request->all(), [
                'code_num' => 'required|string|max:255|unique:master_unit',
                'code_str' => 'required|string|max:255|unique:master_unit',
                'desc' => 'string|max:255',
            ]);
        } else if ($method == 'update') {
            return Validator::make($request->all(), [
                'code_num' => 'required|string|max:255|unique:master_unit,code_num,' . $id . ',id',
                'code_str' => 'required|string|max:255|unique:master_unit,code_str,' . $id . ',id',
                'desc' => 'string|max:255',
            ]);
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $units = MasterUnit::all();
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeCollection(true, $message, $units);

        return response()->json($resource, $statusCode);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->getValidator('create', $request);

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));
            $resource = Serializer::serializeItem(true, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        } else {
            $unit = new MasterUnit();
            $unit->code_num = $request->code_num;
            $unit->code_str = $request->code_str;
            $unit->desc = $request->desc;
            $unit->save();
            $message = "Success Add Data";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true, $message, $unit);
            return response()->json($resource, $statusCode);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $unit = MasterUnit::find($id);
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(false, $message, $unit);

        return response()->json($resource, $statusCode);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->getValidator('update', $request, $id);

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));

            $resource = Serializer::serializeItem(false, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        } else {
            $unit = MasterUnit::find($id);
            if ($unit === null) {
                $message = "Unit with id: " . $id . ", does not exist";
                $statusCode = 500;
                $resource = Serializer::serializeItem(false, $message);
                return response()->json($resource, $statusCode);
            }

            if ($request->code_num != null && $request->code_num != '') $unit->code_num = $request->code_num;

            if ($request->desc != "null" && $request->desc != null && $request->desc != '') {
                $unit->desc = $request->desc;
            }

            if ($request->code_str != "null" && $request->code_str != null && $request->code_str != '') {
                $unit->code_str = $request->code_str;
            }

            $unit->save();

            $message = "success";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true, $message);
            return response()->json($resource, $statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $unituser = MasterUnit::find($id);
        if ($unituser === null) {
            $message = "Unit with id: " . $id . ", does not exist";
            $statusCode = 500;
            $resource = Serializer::serializeItem(false, $message);
            return response()->json($resource, $statusCode);
        }
        $unituser->delete();
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(true, $message);
        return response()->json($resource, $statusCode);
    }
}
