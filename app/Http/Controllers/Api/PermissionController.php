<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\models\Permission;
use App\models\Role;
use Validator;
use App\Http\Library\Serializer;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeCollection(true, $message, $permissions);

        return response()->json($resource, $statusCode);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $permission = Permission::find($id);
        $message = "success";
        $statusCode = 200;
        $resource = Serializer::serializeItem(true, $message, $permission);

        return response()->json($resource, $statusCode);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function attachPermission(Request $request)
    {
        $validator = $this->getValidator('attach', $request);
        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));

            $resource = Serializer::serializeItem(false, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        } else {
            $role = Role::find($request->role_id);
            $permission = Permission::find($request->permission_id);
            $role->attachPermission($permission);

            $message = "Success";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true, $message);
            return response()->json($resource, $statusCode);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detachPermission(Request $request)
    {
        $validator = $this->getValidator('detach', $request);

        if ($validator->fails()) {
            $messages = implode(',', array_column($validator->messages()->toArray(), 0));

            $resource = Serializer::serializeItem(false, $messages);
            $statusCode = 422;
            return response()->json($resource, $statusCode);
        } else {
            $role = Role::find($request->role_id);
            $permission = Permission::find($request->permission_id);
            $role->detachPermission($permission);

            $message = "Success";
            $statusCode = 200;
            $resource = Serializer::serializeItem(true, $message);
            return response()->json($resource, $statusCode);
        }
    }

    public function authorizationList($role_id)
    {
        //check
        if (Role::find($role_id) === null) {
            $message = "Role with id: " . $role_id . ", does not exist";
            $statusCode = 500;
            $resource = Serializer::serializeItem(false, $message);
            return response()->json($resource, $statusCode);
        }


        $query = "SELECT z.id,z.name, IF(a.name IS NULL,0,1) as show_bool, a.id as \"show\", IF(b.name IS NULL,0,1) as create_bool ,
        b.id as \"create\",IF(c.name IS NULL,0,1) as update_bool, c.id as \"update\",IF(d.name IS NULL,0,1) delete_bool, d.id as \"delete\" 
        from modules z
        left join
        (select a.id,a.display_name, IF(b.permission_id IS NULL,null,a.name) as name,a.module_id from (select id, name, module_id, display_name from permissions where name like 'show%' ) a
        left join permission_role b on a.id = b.permission_id and role_id = " . $role_id . ") a on z.id = a.module_id
        left join
        (select a.id,a.display_name, IF(b.permission_id IS NULL,null,a.name) as name,a.module_id from (select id, name, module_id, display_name from permissions where name like 'create%' ) a
        left join permission_role b on a.id = b.permission_id and role_id = " . $role_id . ") b on z.id = b.module_id
        left join
        (select a.id,a.display_name, IF(b.permission_id IS NULL,null,a.name) as name,a.module_id from (select id, name, module_id, display_name from permissions where name like 'update%' ) a
        left join permission_role b on a.id = b.permission_id and role_id = " . $role_id . ") c on z.id = c.module_id
        left join
        (select a.id,a.display_name, IF(b.permission_id IS NULL,null,a.name) as name,a.module_id from (select id, name, module_id, display_name from permissions where name like 'delete%' ) a
        left join permission_role b on a.id = b.permission_id and role_id = " . $role_id . ") d on z.id = d.module_id";


        $query .= " order by z.id asc";
        $data = DB::select(DB::raw($query));

        //Remap Data For Show In Datatable
        $data = collect($data)->map(function ($v, $k) {
            $v->no = ++$k;
            $checked_show = ($v->show_bool == 1) ? " CHECKED " : "";
            $checked_update = ($v->update_bool == 1) ? " CHECKED " : "";
            $checked_create = ($v->create_bool == 1) ? " CHECKED " : "";
            $checked_delete = ($v->delete_bool == 1) ? " CHECKED " : "";

            $disabled_show = empty($v->show) ? " DISABLED " : "";
            $disabled_update = empty($v->update) ? " DISABLED " : "";
            $disabled_create = empty($v->create) ? " DISABLED " : "";
            $disabled_delete = empty($v->delete) ? " DISABLED " : "";

            $disabled_show_class_label = empty($v->show) ? " checkbox-disabled " : "";
            $disabled_update_class_label = empty($v->update) ? " checkbox-disabled " : "";
            $disabled_create_class_label = empty($v->create) ? " checkbox-disabled " : "";
            $disabled_delete_class_label = empty($v->delete) ? " checkbox-disabled " : "";

            $el_checkbox_show = "<label class=\"checkbox checkbox-lg $disabled_show_class_label\">
                                <input type=\"checkbox\" " . $checked_show . " " . $disabled_show . " class=\"checkboxPerm\" value=\" " . $v->show . " \">
                                <span></span>
                            </label>";
            $el_checkbox_update = "<label class=\"checkbox checkbox-lg $disabled_update_class_label\">
                                <input type=\"checkbox\" " . $checked_update . " " . $disabled_update . " class=\"checkboxPerm\" value=\" " . $v->update . " \">
                                <span></span>
                            </label>";
            $el_checkbox_create = "<label class=\"checkbox checkbox-lg $disabled_create_class_label\">
                                <input type=\"checkbox\" " . $checked_create . " " . $disabled_create . " class=\"checkboxPerm\" value=\" " . $v->create . " \">
                                <span></span>
                            </label>";
            $el_checkbox_delete = "<label class=\"checkbox checkbox-lg $disabled_delete_class_label\">
                                <input type=\"checkbox\" " . $checked_delete . " " . $disabled_delete . " class=\"checkboxPerm\" value=\" " . $v->delete . " \">
                                <span></span>
                            </label>";

            $v->checked_show = $el_checkbox_show;
            $v->checked_update = $el_checkbox_update;
            $v->checked_create = $el_checkbox_create;
            $v->checked_delete = $el_checkbox_delete;

            return $v;
        });

        $statusCode = Response::HTTP_OK;
        $message = "Success Get Data";
        $resource = Serializer::serializeItem(true, $message, $data);
        return response()->json($resource, $statusCode);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    private function getValidator($method, Request $request, $id = null)
    {
        if ($method == 'attach') {
            return Validator::make($request->all(), [
                // 'permission_id' => ['required',
                // Role::unique('permissions')->where(function($query){
                //     return $query->where('permission_id',$request->permission_id)
                //                 ->where('role_id',$role_id);
                // })],
                // 'role_id' => 'required|string|max:255',
            ]);
        } else if ($method == 'detach') {
            return Validator::make($request->all(), [
                // 'permission_id' => ['required',
                // Role::unique('permissions')->where(function($query){
                //     return $query->where('permission_id',$request->permission_id)
                //                 ->where('role_id',$role_id);
                // })],
                // 'role_id' => 'required|string|max:255',
            ]);
        }
    }
}
