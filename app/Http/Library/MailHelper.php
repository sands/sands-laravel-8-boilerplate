<?php

namespace App\Http\Library;

use Illuminate\Http\Request;
use App\Email;
use Illuminate\Support\Facades\Mail;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Mail\CustomEmail;



class MailHelper
{
    /**
     * 
     *
     * @param string $baseUrl
     */
    public function __construct(){
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function sendEmail(CustomEmail $mailable, $subject, $to, $type, $ref=null,$content=null, $cc=null,$bcc=null)
    {
        //save email to db
        $email = new Email;
        $email->from = !empty(env('MAIL_USERNAME')) ? env('MAIL_FROM_ADDRESS') : "ims.mnccontent@mncgroup.com";
        $email->to = $to;
        $email->subject = $subject;
        $email->type = $type;
        if($ref!=null){
            $email->reference = $ref;
        }
        if($content!=null){
            $email->content = $content;
        }
        if($cc!=null){
            $email->cc = json_encode($cc);
        }
        if($bcc!=null){
            $email->bcc = json_encode($bcc);
        }
        $email->created_by = JWTAuth::parseToken()->authenticate()->username;
        
        $email->save();

        //send email
        $mail = Mail::to($to);
        if($cc!=null){
            for($i =0; $i< count($cc);$i++){
                $cc[$i] = trim($cc[$i]);
            }
            $mail->cc($cc);
        }
        if($bcc!=null){
            $mail->bcc($bcc);
        }    
        $mail->send($mailable);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function getMailByType($type,$ref=null)
    {
        $emails = Email::where('type',$type);
        if($ref!= null){
            $emails = $emails->where('reference',$ref);
        }
        $emails = $emails->get();

        return $emails;
    }

    public static function getQueryMailByType($type,$ref=null)
    {
        $query = Email::where('type',$type);
        if($ref!= null){
            $query = $query->where('reference',$ref);
        }

        return $query;
    }
}
