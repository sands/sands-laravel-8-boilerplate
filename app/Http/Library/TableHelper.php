<?php

namespace App\Http\Library;

use Illuminate\Http\Request;


class TableHelper
{

    /**
     * 
     *
     * @param string $baseUrl
     */
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function getKeys($queryStrings=[],$keyword)
    {
        foreach($queryStrings as $key=> $item){
            
            if(strpos($key,$keyword)==0){
                $temp = str_replace($keyword,'',$key);
                $queryStrings[$temp] = $item;
                unset($queryStrings[$key]);
            }else{
                unset($queryStrings[$key]);
            }
            
        }
        

        return $queryStrings;
    }


    
}
