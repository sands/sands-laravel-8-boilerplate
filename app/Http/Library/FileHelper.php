<?php

namespace App\Http\Library;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Program;
use App\Episode;
use App\MediaType;
use App\MediaProgram;
use App\MediaEpisode;


class FileHelper
{


    ///PROGRAM directories
    // storage/program/{program_id}/{media_type}/P_{program_id}_{timestamp}
    
    ///EPISODE directories
    // storage/episode/{episode_id}/{media_type}/P_{program_id}_{timestamp}

    /**
     * 
     *
     * @param string $baseUrl
     */
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    // For picture that uploaded in production site: which mean at 172.18.11.14, and saved under folder inside that ip, picture would not to appear. But why in dev it could? 172.18.11.14 being set completely different than 172.17.20.243: since 172.17.20.243 we could access directory directly and the production one not. Just try access 172.18.11.14, it throw to the Application display. So, how to access the picture will never through the same way as the development one. 

    public static function uploadProgram(Program $program,MediaType $media,$files)
    {
        $valid_size = self::validationSizeFilesUpload($files, $media);
        if($valid_size['status'] == false){
            return $valid_size;
        }

        $uploadedFiles=[];
        //set filename and directory path
        foreach($files as $file){
            $filename="P";
            $dirPath="program";
            $filename= $filename.'_'.$program->id_program.'_'.$file->hashName();
            $dirPath=$dirPath.'/'.$program->id_program.'/'.$media->name;
            Storage::putFileAs('public/'.$dirPath,$file,$filename);
            //insert to db
            $mediaProgram = new MediaProgram;
            $mediaProgram->program_id = $program->id_program;
            $mediaProgram->media_type_id = $media->id_media_type;
            $mediaProgram->name = 'P_'.$program->id_program.'_'.$file->hashName();

            // $mediaProgram->path = asset('storage/'.$dirPath.'/'.$filename);

            //Sands, 2020-10-16, change path media storage link
            //$mediaProgram->path = '/storage/app/public/'.$dirPath.'/'.$filename;
            $mediaProgram->path = $dirPath . '/' . $filename;

            $mediaProgram->save();
            // dengan [host] ::
            // $sitePath = url('/');]
            // $filePath = preg_replace('/public/', '',$sitePath);
            // $mediaProgram->path = $filePath.'storage/app/public/'.$dirPath.'/'.$filename;
            array_push($uploadedFiles,$mediaProgram);
        }
        return $uploadedFiles;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function uploadEpisode(Episode $episode,MediaType $media,$files)
    {
        $uploadedFiles=[];
        //set filename and directory path
        foreach($files as $file){
            $filename="E";
            $dirPath="episode";
            $filename= $filename.'_'.$episode->id_episode.'_'.$file->hashName();
            $dirPath=$dirPath.'/'.$episode->id_episode.'/'.$media->name;
            Storage::putFileAs('public/'.$dirPath,$file,$filename);
            //insert to db
            $mediaEpisode = new MediaEpisode;
            $mediaEpisode->episode_id = $episode->id_episode;
            $mediaEpisode->media_type_id = $media->id_media_type;
            $mediaEpisode->name = 'P_'.$episode->id_episode.'_'.$file->hashName();

            // $sitePath = url('/');]
            $mediaEpisode->path = '/storage/app/public/'.$dirPath.'/'.$filename;
            $mediaEpisode->save();
            
            array_push($uploadedFiles,$mediaEpisode);
        }
        return $uploadedFiles;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function deleteFileProgram($id_media_program)
    {
        $media = MediaProgram::find($id_media_program);
        //delete file
        $path = str_replace(env("APP_URL")."public/storage","public/",$media->path);
        Storage::delete($path);
        //delete record
        $media->delete();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function deleteFileEpisode($id_media_episode)
    {
        $media = MediaEpisode::find($id_media_episode);
        //delete file
        $path = str_replace(env("APP_URL")."public/storage","public/",$media->path);
        Storage::delete($path);
        //delete record
        $media->delete();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function getFileProgram($id_media_program)
    {
        //delete file
        $media = MediaProgram::find($id_media_program);
        
        //delete record

        return asset('storage/'.$media->path);
        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public static function getFileEpisode($id_media_program)
    {
        //delete file
        $media = MediaProgram::find($id_media_program);
        
        //delete record

        return asset('storage/'.$media->path);
        
    }

    public static function validationSizeFilesUpload(
        $files = null,
        $media = null
    ){
        $result = [
            'status' => true,
            'messages' => ""
        ];
        $max_size_upload = 0;

        if($media->id_media_type == 2) $max_size_upload = 2048000; // 2 MB
        if($media->id_media_type == 1) $max_size_upload = 102400000; // 100 MB

        foreach($files as $file){
            if($file->getSize() > $max_size_upload){
                $result['status'] = false;
                if($media->id_media_type == 2) $result['messages'] = "Maksimum ukuran file poster yang diupload sebesar 2 MB, harap upload kembali dengan file yang sesuai";
                if($media->id_media_type == 1) $result['messages'] = "Maksimum ukuran file trailer yang diupload sebesar 100 MB, harap upload kembali dengan file yang sesuai";
                break;
            }
        }

        return $result;
    }
}
