<?php

namespace App\Traits;

use App\Jobs\SendEmailDeviationJob;
use App\Mail\ScheduleMail;
use App\Mail\SendEmailDeviation;
use App\Models\User;
use Illuminate\Support\Facades\Mail;

trait SendEmailDeviationTrait
{
    public function mailSend(
        $mailData = null,
        $email_to = [],
        $email_cc = [],
        $email_bcc = [],
        $type = null
    ) {
        $email_to = count(array_filter($email_to)) > 0 ? $email_to : [];
        $email_cc = count(array_filter($email_cc)) > 0 ? $email_cc : [];
        $email_bcc = count(array_filter($email_bcc)) > 0 ? $email_bcc : [];

        if (!empty($email_to)) {
            $mailData['email_to'] = $email_to;
            $mailData['email_cc'] = $email_cc;
            $mailData['email_bcc'] = $email_bcc;
            // Mail::send(new SendEmailDeviation($mailData, $type));
            dispatch(new SendEmailDeviationJob($mailData, $type));
        }
    }

    public function sendEmailNotifSendDeviation($adr = null, $department_id_arr = [])
    {
        try {
            $email_pic = User::select('email_receiver')
                ->whereIn('department_id', $department_id_arr)
                ->where('is_received_mail', User::IS_RECEIVED_MAIL_YES)
                ->get()
                ->pluck('email_receiver')
                ->toArray();
            // $email_pic = ['muhammad.arisandi@mncgroup.com']; //sands

            $subject = 'Notification Deviaton';
            $mailData = [
                'subject' => $subject,
                'data' => $adr,
                'server_name' => env('APP_URL', 'http://adr.mncgroup.com/')
            ];

            //Send Mail
            if(count($email_pic) > 0){
                $this->mailSend($mailData, $email_pic, [], [], SendEmailDeviation::JENIS_EMAIL_NOTIF_DEVIATION_TO_DEPARTMENT);
            }
            
        } catch (\Exception $e) {
            return ['status' => false, 'errors' => $e->getMessage()];
        }

        return  ['status' => true, 'errors' => null];
    }
}
