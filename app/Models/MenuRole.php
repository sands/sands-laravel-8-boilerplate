<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuRole extends Model
{
    protected $table = 'menus_role';
    protected $guarded = [];
    protected $primaryKey = 'id';

    public static function boot()
    {
        parent::boot();

        self::creating(function ($model) {
            $model->created_at = date("Y-m-d H:i:s");
        });

        self::updating(function ($model) {
            $model->updated_at = date("Y-m-d H:i:s");
        });
    }

    public function menu()
    {
        return $this->hasOne(Menu::class, 'id', 'menu_id');
    }

    public function attachRoleMenu($id = null, $role_id = null)
    {
        $this->role_id = $role_id;
        $this->menu_id = $id;
        $this->save();
        return true;
    }

    public function detachRoleMenu($id = null, $role_id = null)
    {
        self::where('menu_id', $id)
            ->where('role_id', $role_id)
            ->delete();
        return true;
    }
}
