<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class MasterOccupation extends Model
{
    protected $table = 'master_occupation';
    protected $guarded = [];
    protected $primaryKey = 'id';

    const OCCUPATION_OPERATOR = 1;
    const OCCUPATION_CREWCHIEF = 2;
    const OCCUPATION_SEKRETARIAT = 3;

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->created_by = Auth::user()->username;
        });

        self::updating(function($model){
            $model->updated_by = Auth::user()->username;
        });

    }

}
