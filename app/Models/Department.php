<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Department extends Model
{
    protected $table = 'master_department';
    protected $guarded = [];
    protected $primaryKey = 'id';
    

    public static function boot()
    {
        parent::boot();

        self::creating(function($model){
            $model->created_by = Auth::user()->username;
        });

        self::updating(function($model){
            $model->updated_by = Auth::user()->username;
        });

    }

}
