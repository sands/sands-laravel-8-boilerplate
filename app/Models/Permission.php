<?php

namespace App\Models;

use Zizaco\Entrust\EntrustPermission;

class Permission extends EntrustPermission
{
    public function module(){
        return $this->belongsTo(Module::class,'module_id');
    }
}
