<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $appends = array('full_permissions_str');

    public function getFullPermissionsStrAttribute()
    { 
        $perms = $this->permissions;
        $str = '';
        $key_len = count($perms);
        if($key_len > 0){
            foreach ($perms as $key => $value) {
                // $str .= "sands";
                $str .= ($key_len == $key+1) ? $value->name."" : $value->name.", ";
            }
        }

        return $str;
    }

    public function permissions(){
        return $this->hasMany(Permission::class,'module_id');
    }
}
