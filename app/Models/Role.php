<?php

namespace App\Models;
use Illuminate\Support\Facades\Config;

use Zizaco\Entrust\EntrustRole;

class Role extends EntrustRole
{
    //list of roles
    const ROLE_DEFAULT = 'default';    
    const ROLE_SUPERADMIN = 'super-admin';    
    const ROLE_ADMIN = 'admin';
    const ROLE_GA = 'ga';
    const ROLE_HR = 'hr';
    const ROLE_HSE = 'hse';
    const ROLE_USER_REGULER = 'user-reguler';

    public function users()
    {
        return $this->belongsToMany(
            Config::get('auth.providers.users.model'),
            Config::get('entrust.role_user_table'),
            Config::get('entrust.role_foreign_key'),
            Config::get('entrust.user_foreign_key')
        );
    }

    public function perms()
    {
        return $this->belongsToMany(
            Config::get('entrust.permission')
        // Config::get('entrust.permission_role_table'),
        // Config::get('entrust.role_foreign_key'),
        // Config::get('entrust.permission_foreign_key')
        );
    }

}
